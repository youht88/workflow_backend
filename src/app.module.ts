import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as fs from 'fs';
import * as yaml from 'yaml';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EsModule } from './utils/db/es/es.module';
import { LangchainModule } from './utils/langchain/langchain.module';
import { OpenaiModule } from './utils/openai/openai.module';
import { RedisModule } from './utils/redis/redis.module';
import { ChatModule } from './workflow/chat/chat.module';
import { UserModule } from './workflow/user/user.module';
import { WorkflowModule } from './workflow/workflow.modules';
import { ProjectModule } from './workflow/project/project.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [() => yaml.parse(fs.readFileSync('env/.dev.yaml', 'utf-8'))],
    }),
    LangchainModule,
    OpenaiModule,
    EsModule,
    WorkflowModule,
    UserModule,
    ChatModule,
    RedisModule,
    ProjectModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
