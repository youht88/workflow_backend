import { Client, ClientOptions } from "@elastic/elasticsearch";
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Schema } from "./es.type";

@Injectable()
export class EsService {
  client: Client;
  constructor(private readonly config: ConfigService) {
    console.log(this.config.get("elasticsearch.nodes"));
    const clientOptions: ClientOptions = {
      node: this.config.get("elasticsearch.nodes") ?? "http://127.0.0.1:9200",
    };
    if (this.config.get("elasticsearch.apiKey")) {
      clientOptions.auth = {
        apiKey: this.config.get("elasticsearch.apiKey"),
      };
    } else if (
      this.config.get("elasticsearch.username") &&
      this.config.get("elasticsearch.password")
    ) {
      clientOptions.auth = {
        username: this.config.get("elasticsearch.username"),
        password: this.config.get("elasticsearch.password"),
      };
    }
    this.client = new Client(clientOptions);
  }

  async insert(
    tableName: string,
    docs: Record<string, any> | Record<string, any>[]
  ) {
    if (!Array.isArray(docs)) {
      docs = [docs];
    }
    const operations = docs.flatMap((doc) => [
      { index: { _index: tableName } },
      doc,
    ]);
    const result = await this.client.bulk({ body: operations, refresh: true });
    if (result.errors) {
      return [];
    } else {
      return result.items.map((item) => item.index?._id);
    }
  }
  async delete(tableName: string, id: string) {
    return await this.client.bulk({
      body: [{ delete: { _index: tableName, _id: id } }],
      refresh: true
    });
  }
  async deleteByQuery(tableName: string, query: Record<string, any>) {
    if (Object.keys(query).length == 0) {
      query = { match_all: {} };
    }
    return this.client.deleteByQuery({
      index: tableName,
      body: { query: query },
      refresh: true
    });
  }
  async updateByQuery(
    tableName: string,
    query: Record<string, any>,
    options: { script?: any; setting?: Record<string, any> }
  ) {
    if (Object.keys(query).length == 0) {
      query = { match_all: {} };
    }
    if (options.script) {
      return await this.client.updateByQuery({
        index: tableName,
        body: {
          query: query,
        },
        script: options.script,
        refresh: true,
      });
    }
    if (options.setting) {
      let source = "";
      for (const key of Object.keys(options.setting)) {
        source = source + `ctx._source.${key} = params.${key};`;
      }
      return await this.client.updateByQuery({
        index: tableName,
        body: {
          query: query,
        },
        script: {
          source: source,
          lang: "painless",
          params: options.setting,
        },
        refresh: true
      });
    }
    return {};
  }
  async query(
    tableName: string,
    query: Record<string, any>,
    options?: {
      fields?: string[];
      aggs?: any;
      script_fields?: any;
      direct?: boolean;
      size?: number;
      sort?: any;
      from?: number;
      nestedPath?: string;
    }
  ) {
    if (Object.keys(query).length == 0) {
      query = { match_all: {} };
    }
    const result = await this.client.search({
      index: tableName,
      _source: options?.fields,
      query: query,
      aggs: options?.aggs,
      script_fields: options?.script_fields,
      from: options?.from,
      size: options?.size,
      sort: options?.sort,
    });
    const direct = options?.direct ?? true;
    if (direct) {
      if (options?.aggs) {
        return result.aggregations;
      } else if (options?.script_fields) {
        return result.hits.hits;
      } else if (options?.nestedPath) {
        return result.hits.hits.map((item) => {
          const inner = item.inner_hits[options?.nestedPath].hits.hits;
          return inner.map((doc) => {
            const source = doc._source;
            source["_index"] = doc._index;
            source["_id"] = doc._id;
            source["_nested"] = doc._nested;
            return source;
          });
        });
      } else {
        return result.hits.hits.map((item) => {
          const source = item._source;
          source["_index"] = item._index;
          source["_id"] = item._id;
          source["_score"] = item._score;
          return source;
        });
      }
    }
    return result;
  }
  async countAll(tableName: string, options: { direct: boolean }) {
    const result = await this.client.count({
      index: tableName,
      body: {
        query: {
          match_all: {},
        },
      },
    });
    const direct = options.direct ?? true;
    if (direct) {
      return result.count;
    }
    return result;
  }
  async count(
    tableName: string,
    query: Record<string, any>,
    options: { direct: boolean }
  ) {
    if (Object.keys(query).length == 0) {
      query = { match_all: {} };
    }
    const result = await this.client.count({
      index: tableName,
      query: query,
    });
    const direct = options.direct || true;
    if (direct) {
      return result.count;
    }
    return result;
  }
  async sql(query: string) {
    const result = await this.client.sql.query({ query: query });
    const data = result.rows.map((row) => {
      const obj = {};
      for (let i = 0; i < row.length; i++) {
        obj[result.columns[i].name] = row[i];
      }
      return obj;
    });
    return data;
  }
  async exists(tableName: string) {
    const isExists = await this.client.indices.exists({ index: tableName });
    return isExists;
  }
  async create(tableName: string, schema: Schema) {
    try {
      const properties = {};
      const mappings = schema.mappings;
      const settings = schema.settings ?? {};
      for (const [name, type] of Object.entries(mappings)) {
        if (type == "text") {
          properties[name] = {
            type: type,
            fields: {
              keyword: {
                type: "keyword",
                ignore_above: 256,
              },
            },
          };
        } else if (type == "dense_vector") {
          properties[name] = {
            "type": type,
            "dims": 1536,
            "index": true,
            "similarity": "l2_norm"
          };
        } else {
          properties[name] = { type: type };
        }
      }
      //console.log("es create:", tableName, properties, settings);
      return await this.client.indices.create({
        index: tableName,
        body: {
          mappings: {
            properties: properties,
          },
          settings: {
            index: {
              number_of_shards: settings.shards ?? 1,
              number_of_replicas: settings.replicas ?? 1,
            },
          },
        },
      });
    } catch (e) {
      console.log("error", e);
    }
  }

  async drop(tableName: string) {
    return await this.client.indices.delete({ index: tableName });
  }
  async test(params) {
    const tableName = params.tableName;
    // const schema: Schema = {
    //     name: "text",
    //     class: "keyword",
    //     age: "integer",
    //     address: "text",
    // };
    const schema: Schema = {
      mappings: {
        exams: "nested",
        name: "keyword",
        content: "text",
      },
      settings: { replicas: 3 },
    };
    // const doc = [
    //     {
    //         name: "youht",
    //         class: "A",
    //         age: 20,
    //         address: "厦门",
    //         friends: ["jinli"],
    //     },
    //     {
    //         name: "jinli",
    //         class: "B",
    //         age: 30,
    //         address: "北京",
    //         friends: ["youht"],
    //     },
    //     { name: "youyc", class: "B", age: 40, address: "上海" },
    // ];
    const doc = [
      {
        name: "name1",
        content: "123",
        exams: [
          { course: "语文", score: 98 },
          { course: "数学", score: 100 },
          { course: "化学", score: 77 },
          { course: "历史", score: 23 },
          { course: "地理", score: 140 },
        ],
      },
      {
        name: "name2",
        content: "456",
        exams: [
          { course: "语文", score: 88 },
          { course: "数学", score: 76 },
        ],
      },
      {
        name: "name3",
        content: "777",
        exams: [
          { course: "语文", score: 20 },
          { course: "数学", score: 50 },
        ],
      },
      {
        name: "name4",
        content: "888",
        exams: [
          { course: "语文", score: 110 },
          { course: "数学", score: 110 },
        ],
      },
      {
        name: "name5",
        content: "999",
        exams: [
          { course: "语文", score: 70 },
          { course: "数学", score: 66 },
        ],
      },
    ];
    // return await this.client.search({
    //     index: `chat_${"cba24098738154ce4820e4c2800acd3c3e7a897f"}`,
    //     query: {
    //         bool: {
    //             must: [{ term: { "conversationId": "fde5f1f2-3100-432a-95a9-b4831495201a" } },
    //             { nested: { path: "chats", query: { match: { "chats.content": "??" } } } }
    //             ]
    //         }
    //     },
    //     sort: [{ "chats.createDateTime": { "order": "asc", "nested": { "path": "chats" } } }],
    // });
    await this.drop(tableName);
    //return await this.exists(tableName);
    await this.create(tableName, schema);
    await this.insert(tableName, doc);
    await this.client.indices.refresh({ index: tableName });
    console.log("&&&&");
    await this.updateByQuery(
      tableName,
      {},
      {
        script: {
          source: `
            ctx._source.exams.removeIf(a->a.score==params.score)
          `,
          params: { score: 70 },
        },
      }
    );
    return this.query(tableName, {});
    // return await this.query(tableName, {}, {
    //     "sort": [
    //         {
    //             "exams.score": {
    //                 "order": "asc",
    //                 "nested": {
    //                     "path": "exams",
    //                     "filter": {
    //                         "term": { "exams.course": "语文" }
    //                     }
    //                 }
    //             }
    //         }
    //     ]
    // });
    // return await this.query(tableName, {}, {
    //     //"size": 1,
    //     "sort": [
    //         { "exams.score": { "order": "asc", "nested": { "path": "exams" } } }
    //     ]
    // });
    // return await this.query(
    //     tableName,
    //     { nested: { path: "exams", query: { match_all: {} }, inner_hits: { size: 10, sort: [{ "exams.score": { "order": "desc" } }] } } },
    //     {
    //         nestedPath: 'exams'
    //         //"size": 1,

    //     });
    //return await this.countAll(tableName);
    //return await this.count(tableName, { "b": "界" });
    //return await this.query(tableName, { "match": { "address": "集美" } });
    //return await this.query(tableName, {});
    //return await this.delete(tableName, '4xa-JooBUvLbdAwfWSat');
    //{"took":35,"errors":false,"items":[{"delete":{"_index":"test4","_id":"4xa-JooBUvLbdAwfWSat","_version":1,"result":"not_found","_shards":{"total":2,"successful":2,"failed":0},"_seq_no":9,"_primary_term":1,"status":404}}]}
    //return await this.deleteByQuery(tableName, { match: { name: 'youht' } });
    //{"took":1015,"timed_out":false,"total":3,"deleted":3,"batches":1,"version_conflicts":0,"noops":0,"retries":{"bulk":0,"search":0},"throttled_millis":0,"requests_per_second":-1,"throttled_until_millis":0,"failures":[]}
    //return await this.query(tableName, { bool: { must: [{ range: { age: { gt: 20 } } }] } }, { fields: ["name", "address"] });
    //return await this.updateByQuery(tableName, { bool: { must: [{ range: { a: { gt: 2 } } }] } }, { setting: { "b": "youht" } });
    //return await this.updateByQuery(tableName, { match: { "name": "jinli" } }, { setting: { "address": "厦门同安", "class": "B" } });
    //{"statusCode":400,"message":"parsing_exception\n\tRoot causes:\n\t\tparsing_exception: [class] query malformed, no start_object after query name"}
    //{"took":137,"timed_out":false,"total":6,"updated":6,"deleted":0,"batches":1,"version_conflicts":0,"noops":0,"retries":{"bulk":0,"search":0},"throttled_millis":0,"requests_per_second":-1,"throttled_until_millis":0,"failures":[]}
    //return await this.sql(`select * from ${tableName} `);
    //return await this.sql(`select sum(a) as x from ${tableName} `);
    return await this.query(
      tableName,
      {},
      {
        aggs: {
          group_by_class: {
            terms: {
              field: "class.keyword",
              size: 10,
            },
            aggs: {
              count: {
                value_count: {
                  field: "class.keyword",
                },
              },
            },
          },
          max_age: {
            max: {
              field: "age",
            },
          },
          average_age: {
            avg: {
              field: "age",
            },
          },
        },
      }
    );
  }
}
