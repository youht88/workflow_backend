import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EsController } from './es.controller';
import { EsService } from './es.service';

@Module({
  imports: [ConfigModule],
  providers: [EsService],
  controllers: [EsController],
  exports: [EsService]
})
export class EsModule { }
