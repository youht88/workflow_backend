import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { EsService } from './es.service';

@Controller('es')
export class EsController {
    constructor(private readonly esService: EsService) { }
    @Get("/test/:tableName")
    async test(@Param() params) {
        return this.esService.test(params);
    }
    @Get("/remove/:tableName")
    async remove(@Param() params) {
        const tableName = params.tableName
        return this.esService.drop(tableName);
    }
    @Post("insert")
    async insert(@Body() body) {
        const tableName = body.tableName;
        const docs = body.docs;
        if (!tableName || !docs) return {};
        return this.esService.insert(tableName, docs);
    }
    @Post("updateByQuery")
    async updateByQuery(@Body() body) {
        const tableName = body.tableName;
        const query = body.query;
        const setting = body.setting;
        const script = body.script;
        if (!tableName || !query) return {};
        return this.esService.updateByQuery(tableName, query, { setting: setting, script: script });
    }
    @Post("deleteByQuery")
    async deleteByQuery(@Body() body) {
        const tableName = body.tableName;
        const query = body.query;
        if (!tableName || !query) return {};
        return this.esService.deleteByQuery(tableName, query);
    }
    @Post("query")
    async query(@Body() body) {
        const tableName = body.tableName;
        const query = body.query;
        const fields = body.fields;
        const aggs = body.aggs;
        const direct = body.direct;
        if (!tableName || !query) return {};
        return this.esService.query(tableName, query, { fields: fields, aggs: aggs, direct: direct });
    }
    @Post("count")
    async count(@Body() body) {
        const tableName = body.tableName;
        const query = body.query;
        const direct = body.direct;
        if (!tableName || !query) return {};
        return this.esService.count(tableName, query, { direct: direct });
    }
    @Post("sql")
    async sql(@Body() body) {
        const query = body.query;
        const direct = body.direct;
        if (!query) return {};
        return this.esService.sql(query);
    }
}

