export type Schema = {
    "mappings": Record<
        string,
        "text" | "keyword" | "long" | "float" | "boolean" | "date" | "nested" | "dense_vector" | "object"
    >, "settings"?: { "shards"?: number, "replicas"?: number }
};