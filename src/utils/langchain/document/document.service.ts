import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Document } from 'langchain/document';
import { CSVLoader } from 'langchain/document_loaders/fs/csv';
import { DirectoryLoader } from 'langchain/document_loaders/fs/directory';
import { JSONLoader } from 'langchain/document_loaders/fs/json';
import { TextLoader } from 'langchain/document_loaders/fs/text';
import { UnstructuredDirectoryLoader, UnstructuredLoader } from 'langchain/document_loaders/fs/unstructured';
import { RecursiveCharacterTextSplitter } from 'langchain/text_splitter';

import * as crypto from 'crypto';
import * as fs from 'fs';
import { compile } from "html-to-text";
import { DocxLoader } from "langchain/document_loaders/fs/docx";
import { PDFLoader } from "langchain/document_loaders/fs/pdf";
import { CheerioWebBaseLoader } from "langchain/document_loaders/web/cheerio";
import { PuppeteerWebBaseLoader, PuppeteerWebBaseLoaderOptions } from "langchain/document_loaders/web/puppeteer";
import { RecursiveUrlLoader, RecursiveUrlLoaderOptions } from "langchain/document_loaders/web/recursive_url";

@Injectable()
export class DocumentService {
    unstructedApiKey: string;
    unstructedApiUrl: string;
    constructor(private readonly config: ConfigService) {
        this.unstructedApiKey = this.config.get('unstructed.apiKey');
        this.unstructedApiUrl = this.config.get('unstructed.apiUrl');
    }
    _getSplitter(chunkSize: number, chunkOverlap: number): RecursiveCharacterTextSplitter {
        const splitter = new RecursiveCharacterTextSplitter({
            chunkSize: chunkSize ?? 500,
            chunkOverlap: chunkOverlap ?? 20,
        });
        return splitter;
    }
    _getSha256(docs: Document<Record<string, any>>[]) {
        //docs[n].pageContent为doc的所有内容
        const docsPageContent = docs.map((doc) => doc.pageContent).join('');
        return crypto.createHash('sha256').update(docsPageContent).digest().toString('hex');
    }
    async _loadAndSplitWithHash(loader, splitter): Promise<Document<Record<string, any>>[]> {
        const docs = await loader.load();
        const hash = this._getSha256(docs);
        const docsSplitted = await splitter.splitDocuments(
            docs,
        );
        docsSplitted.forEach((doc) => doc.metadata["sha256"] = hash);
        return docsSplitted;
    }
    async unstruectedLoad(fileName: string, options?, splitterArgs?: { chunkSize: number, chunkOverlap: number }): Promise<Document<Record<string, any>>[]> {
        options = options ?? {};
        options.apiKey = options.apiKey ?? this.unstructedApiKey;
        options.apiUrl = options.apiUrl ?? this.unstructedApiUrl;

        const loader = new UnstructuredLoader(
            fileName,
            options
        );
        const splitter = this._getSplitter(splitterArgs?.chunkSize ?? 500, splitterArgs?.chunkOverlap ?? 20);
        const docsSplittedWithHash = await this._loadAndSplitWithHash(loader, splitter);
        return docsSplittedWithHash;
    }

    async unstruectedDirectoryLoad(path: string, options?, splitterArgs?: { chunkSize: number, chunkOverlap: number }): Promise<Document<Record<string, any>>[]> {
        options = options ?? {};
        options.apiKey = options.apiKey ?? this.unstructedApiKey;
        options.apiUrl = options.apiUrl ?? this.unstructedApiUrl

        const loader = new UnstructuredDirectoryLoader(
            path,
            options
        );

        const splitter = this._getSplitter(splitterArgs?.chunkSize ?? 500, splitterArgs?.chunkOverlap ?? 20);
        const docsSplittedWithHash = await this._loadAndSplitWithHash(loader, splitter);
        return docsSplittedWithHash;
    }
    async structedDirectoryLoad(directory: string, options?, splitterArgs?: { chunkSize: number, chunkOverlap: number }): Promise<Document<Record<string, any>>[]> {
        options = options ??
        {
            ".json": (path) => new JSONLoader(path),
            ".html": (path) => new TextLoader(path),
            ".txt": (path) => new TextLoader(path),
            ".csv": (path) => new CSVLoader(path),
        };
        const loader = new DirectoryLoader(
            directory,
            options
        );

        const splitter = this._getSplitter(splitterArgs?.chunkSize ?? 500, splitterArgs?.chunkOverlap ?? 20);

        const docsSplittedWithHash = await this._loadAndSplitWithHash(loader, splitter);
        return docsSplittedWithHash;
    }
    async pdfLoad(fileName: string, options?, splitterArgs?: { chunkSize: number, chunkOverlap: number }): Promise<Document<Record<string, any>>[]> {
        let newFileName = "";
        if ((options?.isBase64) ?? false) {
            // 解开base64并存储
            const decodedData = Buffer.from(fileName, 'base64');
            newFileName = `${Date.now()}.pdf`;
            fs.writeFileSync(newFileName, decodedData);
        } else {
            newFileName = fileName;
        }
        const loader = new PDFLoader(
            newFileName,
            options
        );
        const splitter = this._getSplitter(splitterArgs?.chunkSize ?? 500, splitterArgs?.chunkOverlap ?? 20);
        const docsSplittedWithHash = await this._loadAndSplitWithHash(loader, splitter);
        if ((options?.isBase64) ?? false) {
            // 如果是base64文件，则删除
            fs.unlinkSync(newFileName);
        }
        return docsSplittedWithHash;
    }
    async docxLoad(fileName: string, options?, splitterArgs?: { chunkSize: number, chunkOverlap: number }): Promise<Document<Record<string, any>>[]> {
        let newFileName = "";
        if ((options?.isBase64) ?? false) {
            // 解开base64并存储
            const decodedData = Buffer.from(fileName, 'base64');
            newFileName = `${Date.now()}.docx`;
            fs.writeFileSync(newFileName, decodedData);
        } else {
            newFileName = fileName;
        }
        const loader = new DocxLoader(
            newFileName
        );
        const splitter = this._getSplitter(splitterArgs?.chunkSize ?? 500, splitterArgs?.chunkOverlap ?? 20);
        const docsSplittedWithHash = await this._loadAndSplitWithHash(loader, splitter);
        if ((options?.isBase64) ?? false) {
            // 如果是base64文件，则删除
            fs.unlinkSync(newFileName);
        }
        return docsSplittedWithHash;
    }
    async cheerioLoad(url: string, options?, splitterArgs?: { chunkSize: number, chunkOverlap: number }): Promise<Document<Record<string, any>>[]> {
        const loader = new CheerioWebBaseLoader(
            url,
            options
        );
        const splitter = this._getSplitter(splitterArgs?.chunkSize ?? 500, splitterArgs?.chunkOverlap ?? 20);
        const docsSplittedWithHash = await this._loadAndSplitWithHash(loader, splitter);
        return docsSplittedWithHash;
    }
    async puppeteerLoad(url: string, options?: PuppeteerWebBaseLoaderOptions, splitterArgs?: { chunkSize: number, chunkOverlap: number }): Promise<Document<Record<string, any>>[]> {
        options = options ?? {};
        options.launchOptions = options.launchOptions ?? {};
        options.launchOptions.args = [
            "--no-sandbox",
            "--disable-setuid-sandbox"
        ];
        options.launchOptions.executablePath = this.config.get('puppeteer.executablePath');
        const loader = new PuppeteerWebBaseLoader(
            url,
            options
        );
        const splitter = this._getSplitter(splitterArgs?.chunkSize ?? 500, splitterArgs?.chunkOverlap ?? 20);
        const docsSplittedWithHash = await this._loadAndSplitWithHash(loader, splitter);
        return docsSplittedWithHash;
    }
    async websiteLoad(url: string, options?: RecursiveUrlLoaderOptions, splitterArgs?: { chunkSize: number, chunkOverlap: number }): Promise<Document<Record<string, any>>[]> {
        options = options ?? {
            extractor: compile({ wordwrap: 130 }),
            maxDepth: 1,
        }
        const loader = new RecursiveUrlLoader(
            url,
            options,
        );
        const splitter = this._getSplitter(splitterArgs?.chunkSize ?? 500, splitterArgs?.chunkOverlap ?? 20);
        const docsSplittedWithHash = await this._loadAndSplitWithHash(loader, splitter);
        return docsSplittedWithHash;
    }
}

