import { DynamicStructuredTool, DynamicTool } from "langchain/tools";

export function getDynamicStructuredTool({ name, description, schema, func }) {
    if (schema) {
        return new DynamicStructuredTool({
            name: name,
            description: description,
            schema: schema,
            func: func, // Outputs still must be strings
            returnDirect: false, // This is an option that allows the tool to return the output directly
        });
    } else {
        return new DynamicTool({
            name: name,
            description: description,
            func: func, // Outputs still must be strings
            returnDirect: false, // This is an option that allows the tool to return the output directly
        });
    }
}
