import { Module } from '@nestjs/common';
import { ChainVectorDBQAService } from './vector_db_qa_chain.service';

@Module({
  providers: [ChainVectorDBQAService],
  exports: [ChainVectorDBQAService]
})
export class ChainModule { }
