import { Injectable } from '@nestjs/common';
import { VectorDBQAChain } from 'langchain/chains';

@Injectable()
export class ChainVectorDBQAService {
    getVectorDBQAChains(llm, vectorStore, k) {
        const chain = VectorDBQAChain.fromLLM(llm, vectorStore, {
            k: k,
            returnSourceDocuments: true,
        });
        return chain;
    }
}
