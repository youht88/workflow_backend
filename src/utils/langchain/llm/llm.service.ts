import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';
import { ChatBaiduWenxin } from "langchain/chat_models/baiduwenxin";
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
@Injectable()
export class LLMService {
    openaiKeys: string[];
    modelName: string;
    basePath: string;
    constructor(
        private readonly config: ConfigService
    ) {
        this.openaiKeys = this.config.get("openai.apiKeys");
        this.modelName = this.config.get("openai.chat.model");
        this.basePath = this.config.get("openai.basePath");
    }
    _getRandomOpenaiKey(): string {
        return this.openaiKeys[Math.floor(Math.random() * this.openaiKeys.length)];
    }
    getChatWenxin(options: { response?: Response }): ChatBaiduWenxin {
        const response = options.response;
        const key = this.config.get('baidu.baiduApiKey');
        const secretKey = this.config.get('baidu.baiduSecretKey');
        const useCallback = response?.getHeader('Content-Type').toString().includes("text/event-stream");
        let chat: ChatBaiduWenxin;
        console.log("useCallback", useCallback);
        if (response) {
            chat = new ChatBaiduWenxin({
                baiduApiKey: key,
                baiduSecretKey: secretKey,
                streaming: true,
                callbacks: useCallback ? [
                    {
                        handleLLMNewToken(token) {
                            console.log("llm:", token);
                            response.write(token);
                        },
                    }
                ] : null,
            }
            );
        } else {
            chat = new ChatBaiduWenxin({
                baiduApiKey: key,
                baiduSecretKey: secretKey,
            }
            );
        }
        return chat;
    }
    getChatOpenAI(options: { response?: Response }): ChatOpenAI {
        const response = options.response;
        const useCallback = response?.getHeader('Content-Type').toString().includes("text/event-stream");
        const key = this._getRandomOpenaiKey();
        let chat: ChatOpenAI;
        if (response) {
            chat = new ChatOpenAI(
                {
                    temperature: 0.7,
                    modelName: this.modelName,
                    openAIApiKey: key,
                    streaming: true,
                    callbacks: useCallback ? [
                        {
                            handleLLMNewToken(token) {
                                //console.log("llm:", token);
                                response.write(token);
                            },
                        }
                    ] : null,
                },
                { basePath: this.basePath }
            );
        } else {
            chat = new ChatOpenAI(
                {
                    temperature: 0,
                    modelName: this.modelName,
                    openAIApiKey: key,
                    streaming: false,
                },
                { basePath: this.basePath }
            );
        }
        return chat;
    }
    getOpenAIEmbeddings() {
        const key = this._getRandomOpenaiKey();
        return new OpenAIEmbeddings(
            {
                openAIApiKey: key,
            },
            { basePath: this.basePath }
        );
    }
}
