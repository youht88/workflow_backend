import { BaseCallbackHandler, NewTokenIndices } from "langchain/callbacks";
import { Serialized } from "langchain/load/serializable";
import { AgentAction, AgentFinish, ChainValues } from "langchain/schema";

let func;
export class MyCallbackHandler extends BaseCallbackHandler {
  constructor(_func) {
    super();
    func = _func;
  }
  name = "MyCallbackHandler";
  ignoreChain = false;
  ignoreLLM = false;
  async handleChainStart(chain: Serialized) {
    //console.log(`1***Entering new ${chain.id} chain...`);
  }

  async handleChainEnd(_output: ChainValues) {
    //console.log("2***Finished chain.");
  }

  async handleAgentAction(action: AgentAction) {
    console.log("3***", action.log);
    await func(action.log.split("Action:")[0] + '\n\n')
  }

  async handleToolEnd(output: string) {
    //console.log("4***", output);
  }

  async handleText(text: string) {
    //console.log("5***", text);
  }

  async handleAgentEnd(action: AgentFinish) {
    console.log("6***", action.returnValues, action.log);
    await func("<<CLEAR>>" + action.returnValues.output)
  }
  async handleLLMNewToken?(token: string, idx: NewTokenIndices, runId: string, parentRunId?: string | undefined, tags?: string[] | undefined) {
    //console.log("7***", token)
  };

}