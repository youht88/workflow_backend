import { Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
//import { Configuration, OpenAIApi } from 'openai';
//import { OpenAI } from 'langchain/llms/openai';
import * as jieba from "@node-rs/jieba";
//import * as tiktoken from 'js-tiktoken';
import {
  AgentExecutor,
  initializeAgentExecutorWithOptions,
} from "langchain/agents";
import { ConversationChain, LLMChain } from "langchain/chains";
import { ConversationSummaryMemory } from "langchain/memory";
import { StructuredOutputParser } from "langchain/output_parsers";
import {
  ChatPromptTemplate,
  FewShotPromptTemplate,
  HumanMessagePromptTemplate,
  LengthBasedExampleSelector,
  MessagesPlaceholder,
  PromptTemplate,
  SemanticSimilarityExampleSelector,
  SystemMessagePromptTemplate
} from "langchain/prompts";
import { ChatMessage, HumanMessage } from "langchain/schema";
import { Calculator } from "langchain/tools/calculator";
import { HNSWLib } from "langchain/vectorstores/hnswlib";
import z from "zod";
import { getMyAgentExecutor } from "./agents/my_agent";
import { MyCallbackHandler } from "./callbacks/my_callback";
import { ChainVectorDBQAService } from "./chain/vector_db_qa_chain.service";
import { DocumentService } from "./document/document.service";
import { LLMService } from "./llm/llm.service";
import { getDynamicStructuredTool } from "./tools/dynamic_tool";
import { VectorStoreService } from "./vector_store/vector_store.service";
@Injectable()
export class LangchainService {
  logger = new Logger(LangchainService.name);
  //memoryMap 结构为account:{conversationId:{...}}
  memoryMap: Record<string, Record<string, ConversationSummaryMemory>> = {};
  //memoryMap: Record<string, Record<string, BufferWindowMemory>> = {};  
  executorMap: Record<string, Record<string, AgentExecutor>> = {};

  constructor(private readonly config: ConfigService,
    private readonly llmService: LLMService,
    private readonly documentService: DocumentService,
    private readonly vectorStoreService: VectorStoreService,
    private readonly chainVectorDBQAService: ChainVectorDBQAService
  ) { }
  async test_baidu(queryString: string, res) {
    const chat = this.llmService.getChatWenxin({ response: res });
    this.logger.log(`testBaidu:", ${queryString},${chat}`);
    const result = await chat.call([new HumanMessage(queryString)]);
    return result;
  }
  async justTest(url: string, query: string) {
    let docs;
    const chat = this.llmService.getChatOpenAI({});
    const embeddings = this.llmService.getOpenAIEmbeddings();
    const vectorStore = await this.vectorStoreService.getElasticSearchVectorStore('test_embedding6', embeddings);
    const vectorStoreAll = await this.vectorStoreService.getElasticSearchVectorStore('test_embedding*', embeddings);

    if (url != "") {
      // const buff = fs.readFileSync("src/example/ylz2022.pdf");
      // const data64 = buff.toString('base64');
      // const docs = await this.documentService.pdfLoad(data64, { isBase64: true });
      //const docs = await  this.documentService.unstruectedLoad("src/example/ylz2022.pdf");
      //const docs = await this.documentService.cheerioLoad("https://baijiahao.baidu.com/s?id=1777028313750727542");
      //return await this.documentService.websiteLoad("https://js.langchain.com/docs/get_started/introduction");
      const docs = await this.documentService.puppeteerLoad(url);
      //return docs;
      console.log(`start add Documents:${docs[0].metadata["sha256"]},${docs.length}`);
      const ids = await this.vectorStoreService.addDocuments(vectorStore, docs, false);
      console.log(ids);
    }
    const chain = this.chainVectorDBQAService.getVectorDBQAChains(chat, vectorStoreAll, 2);
    //const response = await chain.call({ query: "如何评价中美关系" });
    //const response = await chain.call({ query: "java和python做一下比较" });
    const response = await chain.call({ "query": query });
    return response;
  }
  //根据传入的string,向response不定时地写入相应文字。
  //约定<<CLEAR>>为清空指令
  async fakeStream(text: string, res, lowDelay?, highDelay?) {
    lowDelay = lowDelay || 10;
    highDelay = highDelay || 100;
    //分词
    const parts = text.split("<<CLEAR>>");
    let chunks = [];
    for (const part of parts) {
      chunks = chunks.concat(jieba.cut(part));
      chunks.push("<<CLEAR>>");
    }
    //清除最后push进来的<<CLEAR>>标识
    chunks.pop();
    //不固定延迟
    await new Promise<void>(async (resolve, reject) => {
      for (const chunk of chunks) {
        const delay =
          lowDelay + Math.round(Math.random() * (highDelay - lowDelay));
        console.log("????", chunk, delay)
        await new Promise<void>((resolve, reject) => {
          setTimeout(() => {
            res.write(chunk);
            resolve();
          }, delay);
        });
      }
      resolve();
    });
    return;
  }
  async predict({ query, res, isStream }) {
    const prompt = query.prompt;
    const chat = this.llmService.getChatOpenAI({ response: res });
    return await chat.predict(prompt);
  }
  async test({ body, res, isStream }) {
    const chat = this.llmService.getChatOpenAI({ response: res });
    const embeddings = this.llmService.getOpenAIEmbeddings();
    const messages: Record<string, string>[] = body.messages;
    const account = body.account;
    const conversationId: string = body.conversationId;
    console.log(
      "account:",
      account,
      "conversationId:",
      conversationId,
      "sk:",
      chat.openAIApiKey
    );
    const chatMessages = messages.map((message, index) => {
      return new ChatMessage(message.content, message.role);
    });
    const lastMessage = messages.pop();
    // //test document
    // const docs = await dirLoader(lastMessage.content, 'src/example', embeddings);
    // console.log("docs!!!!!:", docs);
    // return "ok";

    // //test chain
    // const chain = getApiChains(chat);
    // const answer = await chain.call({ question: lastMessage.content });
    // return answer;

    //test agent
    let executor: AgentExecutor = this.executorMap[account]?.[conversationId];
    if (!executor) {
      executor = await initializeAgentExecutorWithOptions(
        [
          new Calculator(),
          getDynamicStructuredTool({
            name: "getLength",
            description: "根据传入的字符串计算该字符串的长度。",
            schema: z
              .object({ text: z.string().describe("传入的字符串") })
              .describe("计算出字符串的长度"),
            func: (text: string) => {
              return `${text.length}`;
            },
          }),
        ],
        chat,
        {
          //agentType: "chat-conversational-react-description",
          agentType: "structured-chat-zero-shot-react-description",
          verbose: false,
        }
      );
      this.executorMap[account] = this.executorMap[account] || {};
      this.executorMap[account][conversationId] = executor;
    }
    const controller = new AbortController();
    setTimeout(() => {
      controller.abort();
    }, 60000);
    try {
      await executor.call({ input: "要求严格使用中文进行一步一步地推理并解答:\n\n" + lastMessage.content, signal: controller.signal }, [
        new MyCallbackHandler(async (text) => await this.fakeStream(text, res))
      ]);
    }
    catch (e) {
      console.log(e);
      await this.fakeStream("不好意思,我的思考超时了😅", res, 0, 50);
    }
    // return await executor.run(lastMessage.content, [
    //   new MyCallbackHandler()
    // ]);
    //return await executor.run(lastMessage.content)
  }
  async agent({ body, res, isStream }) {
    const chat = this.llmService.getChatOpenAI({ response: res });
    const embeddings = this.llmService.getOpenAIEmbeddings();
    const messages: Record<string, string>[] = body.messages;
    const account = body.account;
    const conversationId: string = body.conversationId;
    console.log(
      "account:",
      account,
      "conversationId:",
      conversationId,
      "sk:",
      chat.openAIApiKey
    );
    const chatMessages = messages.map((message, index) => {
      return new ChatMessage(message.content, message.role);
    });
    const lastMessage = messages.pop();

    //test agent
    let executor: AgentExecutor = this.executorMap[account]?.[conversationId];
    if (!executor) {
      const tools = [
        new Calculator(),
        getDynamicStructuredTool({
          name: "getLength",
          description: "根据传入的字符串计算该字符串的长度。",
          schema: z
            .object({ text: z.string().describe("传入的字符串") })
            .describe("计算出字符串的长度"),
          func: (text: string) => {
            return `${text.length}`;
          },
        }),
      ];
      executor = getMyAgentExecutor(
        chat,
        tools
      );
      this.executorMap[account] = this.executorMap[account] || {};
      this.executorMap[account][conversationId] = executor;
    }
    const controller = new AbortController();
    setTimeout(() => {
      controller.abort();
    }, 60000);
    try {
      await executor.call({ input: "要求严格使用中文进行一步一步地推理并解答:\n\n" + lastMessage.content, signal: controller.signal }, [
        new MyCallbackHandler(async (text) => await this.fakeStream(text, res))
      ]);
    }
    catch (e) {
      console.log(e);
      await this.fakeStream("不好意思,我的思考超时了😅", res, 0, 50);
    }
  }

  async getActionUseFewShot(input: string) {
    try {
      const chat = this.llmService.getChatOpenAI({});
      const embeddings = this.llmService.getOpenAIEmbeddings();
      console.log("sk:", chat.openAIApiKey);
      const examplePrompt = new PromptTemplate({
        inputVariables: ["input", "output"],
        template: "Input: {input}\nOutput: {output}",
      });
      const examples = [
        {
          input: "早上吃了个蛋糕,喝了两杯半牛奶",
          output: { type: "diet", mode: "record" },
        },
        {
          input: "下午3点到5点吃了一颗阿司匹林",
          output: { type: "diet", mode: "record" },
        },
        {
          input: "昨天下午在车站3点半喝了50ml水",
          output: { type: "diet", mode: "record" },
        },
        {
          input: "一家三口人这个礼拜花103块抽了2包兰狼烟",
          output: { type: "diet", mode: "record" },
        },
        {
          input: "中午吃了一碗豆角煎鱼,昨天晚上喝了1听可乐,分别花了12块和90块",
          output: { type: "diet", mode: "record" },
        },
        {
          input: "2个小时前吃中药吃了20分钟,在办公室",
          output: { type: "diet", mode: "record" },
        },
        {
          input: "吃了一盘西红柿炒蛋，苦瓜炒豆芽和三盘牛肉蒸蛋",
          output: { type: "diet", mode: "record" },
        },
        {
          input: "早上跑了2公里,下午打了2个小时的羽毛球",
          output: { type: "sport", mode: "record" },
        },
        {
          input: "下午3点到5点在体育馆与同事打了一场篮球",
          output: { type: "sport", mode: "record" },
        },
        {
          input: "昨天下午3点半跳了300下绳,共花了7分半",
          output: { type: "sport", mode: "record" },
        },
        {
          input: "这个礼拜花50块钱跳了两场交谊舞",
          output: { type: "sport", mode: "record" },
        },
        {
          input:
            "中午和小明做了40个俯卧撑,昨天晚上在昌平游泳馆游了1小时35分钟的泳,分别花了12块和90块",
          output: { type: "sport", mode: "record" },
        },
        {
          input: "2个小时前在狐尾山爬山，爬了45分钟",
          output: { type: "sport", mode: "record" },
        },
        {
          input: "体重77公斤,身高1米74",
          output: { type: "sign", mode: "record" },
        },
        {
          input: "下午3点到5点在公司,测量心跳91",
          output: { type: "sign", mode: "record" },
        },
        {
          input: "昨天下午3点半饭前血糖4.5",
          output: { type: "sign", mode: "record" },
        },
        {
          input: "15分钟前量血压分别是90和120",
          output: { type: "sign", mode: "record" },
        },
        {
          input: "上周五下午体重有158斤,今天早上减了3公斤",
          output: { type: "sign", mode: "record" },
        },
        {
          input: "早上写了2个小时的作业,下午在公司做了一个ppt",
          output: { type: "behavior", mode: "record" },
        },
        {
          input: "下午3点到5点在学校与3个同学和老师开了2个小时的研讨会",
          output: { type: "behavior", mode: "record" },
        },
        {
          input: "昨天下午3点半打游戏,和女朋友共花了7分半",
          output: { type: "behavior", mode: "record" },
        },
        {
          input: "这个礼拜花50块钱买了三件衣服",
          output: { type: "behavior", mode: "record" },
        },
        {
          input:
            "前天中午和小明打牌打到晚上11点15,昨天晚上在家刷抖音刷了3个小时,分别花了12块和90块",
          output: { type: "behavior", mode: "record" },
        },
        {
          input: "2个小时前在花都百货花了45块大洋买了一个鼠标",
          output: { type: "behavior", mode: "record" },
        },
        {
          input: "昨天左肩有点疼,今天更疼了",
          output: { type: "feel", mode: "record" },
        },
        {
          input: "下午3点到5点在公司,心跳有点快",
          output: { type: "feel", mode: "record" },
        },
        {
          input: "昨天下午3点半头皮一直很痒",
          output: { type: "feel", mode: "record" },
        },
        {
          input: "15分钟前心情很郁闷",
          output: { type: "feel", mode: "record" },
        },
        {
          input: "上周五下午腹胀,今天早上背部很酸胀",
          output: { type: "feel", mode: "record" },
        },
        {
          input: "前天在医务室腰酸了半个小时",
          output: { type: "feel", mode: "record" },
        },
      ];
      const examplesFix = examples.map((item) => {
        return {
          input: item.input,
          output: `${JSON.stringify(item.output)
            .replace(/{/g, "{{")
            .replace(/}/g, "}}")}`,
        };
      });
      // Create a LengthBasedExampleSelector that will be used to select the examples.
      const exampleSelector =
        await SemanticSimilarityExampleSelector.fromExamples(
          examplesFix,
          embeddings,
          HNSWLib,
          {
            k: 5,
          }
        );

      const samplePrompt = new FewShotPromptTemplate({
        // We provide an ExampleSelector instead of examples.
        exampleSelector,
        examplePrompt,
        prefix: "把句子解释成json数据",
        suffix: "Input: {input}\nOutput: ",
        inputVariables: ["input"],
      });

      const chatPrompt = ChatPromptTemplate.fromPromptMessages([
        new HumanMessagePromptTemplate(samplePrompt),
      ]);
      const chain = new LLMChain({
        prompt: chatPrompt,
        llm: chat,
        verbose: false,
      });
      const result = await chain.call({ input });
      console.log("result:", result.text);

      return JSON.parse(result.text);
    } catch (e) {
      this.logger.error(e);
      return {};
    }
  }
  async getActionUseOutputParser(input: string) {
    const chat = this.llmService.getChatOpenAI({});
    console.log("sk:", chat.openAIApiKey);
    const actionParser = StructuredOutputParser.fromZodSchema(
      z
        .object({
          type: z.enum(["sport", "diet", "sign", "feel", "behavior", "other"])
            .describe(`
        识别语句与什么情形相关:
        \n如果与运动有关,如"打篮球",则为sport;
        \n如果与饮食有关则为diet,常用动词有吃、喝、干、服等如:
        \n - 服用药物
        \n - 吃蛋糕
        \n - 喝牛奶
        \n - 吃药
        \n - 抽烟
        \n - 喝酒;
        \n如果与身体的体征指标有关,如"血压、身高、体重、血糖、视力、三围等",则为sign;
        \n如果与身体的感觉或情绪感知有关,如"肚子有点疼","头疼得厉害",则为feel;
        \n如果与某个行为有关但又不是运动则为behavior,如
        \n - 买衣服
        \n - 写作业
        \n - 开会
        \n - 洗衣服;
        \n其他情况则为other"),
        `),
          mode: z.enum(["query", "record", "plan", "other"]).describe(`
        "识别语句与什么目的相关:
        \n如果与查询自己某些数据有关,如:"吃拉面花了多少钱","昨天几点打的篮球",则为query;
        \n如果与表述某个事实或自己的身体状况有关,常用"了"或"是"、"为"表示,如:"血压为120","买了一个键盘","跑了2公里","吃了个苹果","体重是70","肚子有点疼"则为record;
        \n如果与自己的某项计划,某个目标有关,如:"过15分钟要开个会","打算下个礼拜去北京","下周抽烟不超过2包"则为plan;
        \n其他情况则为other")
       `),
        })
        .required({ type: true, mode: true })
    );
    const formatInstructions = actionParser.getFormatInstructions();
    const actionPrompt = new PromptTemplate({
      template:
        "快速分析语句的意图,不要任何解释,不确定则直接输出<mode=other,type=other>直接按以下规则解析\n{format_instructions}\n{input}",
      inputVariables: ["input"],
      partialVariables: { format_instructions: formatInstructions },
    });
    const chatPrompt = (
      await actionPrompt.formatPromptValue({ input })
    ).toChatMessages();
    const result = await chat.call(chatPrompt);
    try {
      return JSON.parse(result.content);
    } catch (e) {
      return { mode: "other", type: "other" };
    }
  }
  async _getDietFewShot(): Promise<FewShotPromptTemplate> {
    const examplePrompt = new PromptTemplate({
      inputVariables: ["input", "output"],
      template: "Input: {input}\nOutput: {output}",
    });
    const examples = [
      {
        input: "早上吃了个蛋糕,喝了两杯半牛奶",
        output: [
          { stime: "早上", act: "吃", name: "蛋糕", value: 1, unit: "个" },
          { stime: "早上", act: "喝", name: "牛奶", value: 2.5, unit: "杯" },
        ],
      },
      {
        input: "下午3点到5点吃了一颗阿司匹林",
        output: [
          {
            stime: "下午3点",
            etime: "下午5点",
            act: "吃",
            name: "阿司匹林",
            value: 1,
            unit: "颗",
            drug: true,
          },
        ],
      },
      {
        input: "昨天下午在车站3点半喝了50ml水",
        output: [
          {
            stime: "昨天下午3点半",
            place: "车站",
            act: "喝",
            name: "水",
            value: 50,
            unit: "ml",
            water: true,
          },
        ],
      },
      {
        input: "一家三口人这个礼拜花103块抽了2包兰狼烟",
        output: [
          {
            person: "我们一家三口人",
            stime: "这个礼拜",
            act: "抽",
            name: "兰狼烟",
            value: 2,
            unit: "包",
            cost: 103,
            smoke: true,
          },
        ],
      },
      {
        input: "中午吃了一碗豆角煎鱼,昨天晚上喝了1听可乐,分别花了12块和90块",
        output: [
          {
            stime: "中午",
            cost: 12,
            act: "吃",
            name: "豆角煎鱼",
            value: 1,
            unit: "碗",
            homedish: true,
          },
          {
            stime: "昨天晚上",
            cost: 90,
            act: "喝",
            name: "可乐",
            value: 1,
            unit: "听",
          },
        ],
      },
      {
        input: "2个小时前吃中药吃了20分钟,在办公室",
        output: [
          {
            place: "办公室",
            stime: "2个小时前",
            duration: "20分钟",
            act: "吃",
            name: "中药",
            value: 1,
            unit: null,
            drug: true,
          },
        ],
      },
      {
        input: "吃了一盘西红柿炒蛋，苦瓜炒豆芽和三盘牛肉蒸蛋",
        output: [
          {
            act: "吃",
            name: "西红柿炒蛋",
            value: 1,
            unit: "盘",
            homedish: true,
          },
          {
            act: "吃",
            name: "苦瓜炒豆芽",
            value: 1,
            unit: "盘",
            homedish: true,
          },
          { act: "吃", name: "牛肉蒸蛋", value: 3, unit: "盘", homedish: true },
        ],
      },
    ];
    const examplesFix = examples.map((item) => {
      return {
        input: item.input,
        output: `${JSON.stringify(item.output)
          .replace(/{/g, "{{")
          .replace(/}/g, "}}")}`,
      };
    });
    // Create a LengthBasedExampleSelector that will be used to select the examples.
    const exampleSelector = await LengthBasedExampleSelector.fromExamples(
      examplesFix,
      {
        examplePrompt,
        maxLength: 25,
      }
    );
    const samplePrompt = new FewShotPromptTemplate({
      // We provide an ExampleSelector instead of examples.
      exampleSelector,
      examplePrompt,
      prefix: "把句子解释成json数据。如果是家常菜请标识homedish为true",
      suffix: "Input: {input}\nOutput:",
      inputVariables: ["input"],
    });
    return samplePrompt;
  }
  async _getSportFewShot(): Promise<FewShotPromptTemplate> {
    const examplePrompt = new PromptTemplate({
      inputVariables: ["input", "output"],
      template: "Input: {input}\nOutput: {output}",
    });
    const examples = [
      {
        input: "早上跑了2公里,下午打了2个小时的羽毛球",
        output: [
          { stime: "早上", name: "跑步", distance: 2, distanceUnit: "公里" },
          { stime: "下午", duration: "2个小时", act: "打", name: "羽毛球" },
        ],
      },
      {
        input: "下午3点到5点在体育馆与同事打了一场篮球",
        output: [
          {
            stime: "下午3点",
            etime: "下午5点",
            place: "体育馆",
            person: "我和同事",
            act: "打",
            name: "篮球",
            value: 1,
            unit: "场",
          },
        ],
      },
      {
        input: "昨天下午3点半跳了300下绳,共花了7分半",
        output: [
          {
            stime: "昨天下午3点半",
            duration: "7.5分钟",
            act: "跳",
            name: "绳",
            value: 300,
            unit: "下",
          },
        ],
      },
      {
        input: "这个礼拜花50块钱跳了两场交谊舞",
        output: [
          {
            stime: "这个礼拜",
            act: "跳",
            name: "交谊舞",
            value: 2,
            unit: "场",
            cost: 50,
          },
        ],
      },
      {
        input:
          "中午和小明做了40个俯卧撑,昨天晚上在昌平游泳馆游了1小时35分钟的泳,分别花了12块和90块",
        output: [
          {
            stime: "中午",
            person: "我和小明",
            cost: 12,
            act: "做",
            name: "俯卧撑",
            value: 40,
            unit: "个",
          },
          {
            stime: "昨天晚上",
            place: "昌平游泳馆",
            cost: 90,
            name: "游泳",
            duration: "95分钟",
          },
        ],
      },
      {
        input: "2个小时前在狐尾山爬山，爬了45分钟",
        output: [
          {
            stime: "2个小时前",
            duration: "45分钟",
            name: "爬山",
            place: "狐尾山",
          },
        ],
      },
    ];
    const examplesFix = examples.map((item) => {
      return {
        input: item.input,
        output: `${JSON.stringify(item.output)
          .replace(/{/g, "{{")
          .replace(/}/g, "}}")}`,
      };
    });
    // Create a LengthBasedExampleSelector that will be used to select the examples.
    const exampleSelector = await LengthBasedExampleSelector.fromExamples(
      examplesFix,
      {
        examplePrompt,
        maxLength: 25,
      }
    );
    const samplePrompt = new FewShotPromptTemplate({
      // We provide an ExampleSelector instead of examples.
      exampleSelector,
      examplePrompt,
      prefix: "把句子解释成json数据",
      suffix: "Input: {input}\nOutput: ",
      inputVariables: ["input"],
    });
    return samplePrompt;
  }
  async _getSignFewShot(): Promise<FewShotPromptTemplate> {
    const examplePrompt = new PromptTemplate({
      inputVariables: ["input", "output"],
      template: "Input: {input}\nOutput: {output}",
    });
    const examples = [
      {
        input: "体重77公斤,身高1米74",
        output: [
          { name: "体重", value: 77, unit: "公斤" },
          { name: "身高", value: 174, unit: "cm" },
        ],
      },
      {
        input: "下午3点到5点在公司,测量心跳91",
        output: [
          {
            stime: "下午3点",
            etime: "下午5点",
            place: "公司",
            name: "心跳",
            value: 91,
            unit: "次/分钟",
          },
        ],
      },
      {
        input: "昨天下午3点半饭前血糖4.5",
        output: [
          {
            stime: "昨天下午3点半",
            name: "饭前血糖",
            value: 4.5,
            unit: "摩尔/升",
          },
        ],
      },
      {
        input: "15分钟前量血压分别是90和120",
        output: [
          { stime: "15分钟前", name: "舒张压", value: 90, unit: "毫米汞柱" },
          { stime: "15分钟前", name: "收缩压", value: 120, unit: "毫米汞柱" },
        ],
      },
      {
        input: "上周五下午体重有158斤,今天早上减了3公斤",
        output: [
          { stime: "上周五下午", name: "体重", value: 158, unit: "斤" },
          { stime: "今天早上", name: "体重", value: 152, unit: "斤" },
        ],
      },
      {
        input: "左眼5.1,右眼4.2。三围分别是45.6、35.7和50.5",
        output: [
          { name: "左眼视力", value: 5.1, unit: "度" },
          { name: "右眼视力", value: 4.2, unit: "度" },
          { name: "胸围", value: 45.6, unit: "cm" },
          { name: "腰围", value: 35.7, unit: "cm" },
          { name: "臀围", value: 50.5, unit: "cm" },
        ],
      },
    ];
    const examplesFix = examples.map((item) => {
      return {
        input: item.input,
        output: `${JSON.stringify(item.output)
          .replace(/{/g, "{{")
          .replace(/}/g, "}}")}`,
      };
    });
    // Create a LengthBasedExampleSelector that will be used to select the examples.
    const exampleSelector = await LengthBasedExampleSelector.fromExamples(
      examplesFix,
      {
        examplePrompt,
        maxLength: 25,
      }
    );
    const samplePrompt = new FewShotPromptTemplate({
      // We provide an ExampleSelector instead of examples.
      exampleSelector,
      examplePrompt,
      prefix: "把句子解释成json数据",
      suffix: "Input: {input}\nOutput:",
      inputVariables: ["input"],
    });
    return samplePrompt;
  }
  async _getBehaviorFewShot(): Promise<FewShotPromptTemplate> {
    const examplePrompt = new PromptTemplate({
      inputVariables: ["input", "output"],
      template: "Input: {input}\nOutput: {output}",
    });
    const examples = [
      {
        input: "早上写了2个小时的作业,下午在公司做了一个ppt",
        output: [
          { stime: "早上", act: "写", name: "作业", duration: "2个小时" },
          { stime: "下午", act: "做", name: "ppt", place: "公司" },
        ],
      },
      {
        input: "下午3点到5点在学校与3个同学和老师开了2个小时的研讨会",
        output: [
          {
            stime: "下午3点",
            etime: "下午5点",
            place: "学校",
            person: "我和3个同学以及老师",
            act: "开",
            name: "研讨会",
            value: 1,
            unit: "场",
          },
        ],
      },
      {
        input: "昨天下午3点半打游戏,和女朋友共花了7分半",
        output: [
          {
            stime: "昨天下午3点半",
            duration: "7.5分钟",
            act: "打",
            name: "游戏",
            person: "我和女朋友",
          },
        ],
      },
      {
        input: "这个礼拜花50块钱买了三件衣服",
        output: [
          {
            stime: "这个礼拜",
            act: "买",
            name: "衣服",
            value: 3,
            unit: "件",
            cost: 50,
          },
        ],
      },
      {
        input:
          "前天中午和小明打牌打到晚上11点15,昨天晚上在家刷抖音刷了3个小时,分别花了12块和90块",
        output: [
          {
            stime: "前天中午",
            etime: "前天晚上11点15",
            person: "我和小明",
            cost: 12,
            act: "打",
            name: "扑克",
            value: 40,
            unit: "个",
          },
          {
            stime: "昨天晚上",
            place: "家",
            cost: 90,
            act: "刷",
            name: "抖音",
            duration: "180分钟",
          },
        ],
      },
      {
        input: "2个小时前在花都百货花了45块大洋买了一个鼠标",
        output: [
          {
            stime: "2个小时前",
            act: "买",
            name: "鼠标",
            place: "花都百货",
            cost: 45,
          },
        ],
      },
    ];
    const examplesFix = examples.map((item) => {
      return {
        input: item.input,
        output: `${JSON.stringify(item.output)
          .replace(/{/g, "{{")
          .replace(/}/g, "}}")}`,
      };
    });
    // Create a LengthBasedExampleSelector that will be used to select the examples.
    const exampleSelector = await LengthBasedExampleSelector.fromExamples(
      examplesFix,
      {
        examplePrompt,
        maxLength: 25,
      }
    );
    const samplePrompt = new FewShotPromptTemplate({
      // We provide an ExampleSelector instead of examples.
      exampleSelector,
      examplePrompt,
      prefix: "把句子解释成json数据",
      suffix: "Input: {input}\nOutput:",
      inputVariables: ["input"],
    });
    return samplePrompt;
  }
  async _getFeelFewShot(): Promise<FewShotPromptTemplate> {
    const examplePrompt = new PromptTemplate({
      inputVariables: ["input", "output"],
      template: "Input: {input}\nOutput: {output}",
    });
    const examples = [
      {
        input: "昨天左肩有点疼,今天更疼了",
        output: [
          { stime: "昨天", name: "左肩", value: "有点疼" },
          { stime: "今天", name: "左肩", value: "疼的厉害" },
        ],
      },
      {
        input: "下午3点到5点在公司,心跳有点快",
        output: [
          {
            stime: "下午3点",
            etime: "下午5点",
            place: "公司",
            name: "心跳",
            value: "有点快",
          },
        ],
      },
      {
        input: "昨天下午3点半头皮一直很痒",
        output: [{ stime: "昨天下午3点半", name: "头皮", value: "很痒" }],
      },
      {
        input: "15分钟前心情很郁闷",
        output: [{ stime: "15分钟前", name: "心情", value: "郁闷" }],
      },
      {
        input: "上周五下午腹胀,今天早上背部很酸胀",
        output: [
          { stime: "上周五下午", name: "腹部", value: "胀" },
          { stime: "今天早上", name: "背部", value: "很酸胀" },
        ],
      },
      {
        input: "前天在医务室腰酸了半个小时",
        output: [
          {
            stime: "前天",
            place: "医务室",
            name: "腰",
            value: "酸",
            duration: "半个小时",
          },
        ],
      },
    ];
    const examplesFix = examples.map((item) => {
      return {
        input: item.input,
        output: `${JSON.stringify(item.output)
          .replace(/{/g, "{{")
          .replace(/}/g, "}}")}`,
      };
    });
    // Create a LengthBasedExampleSelector that will be used to select the examples.
    const exampleSelector = await LengthBasedExampleSelector.fromExamples(
      examplesFix,
      {
        examplePrompt,
        maxLength: 25,
      }
    );
    const samplePrompt = new FewShotPromptTemplate({
      // We provide an ExampleSelector instead of examples.
      exampleSelector,
      examplePrompt,
      prefix: "把句子解释成json数据",
      suffix: "Input: {input}\nOutput:",
      inputVariables: ["input"],
    });
    return samplePrompt;
  }

  _getDietParser() {
    const dietParser = StructuredOutputParser.fromZodSchema(
      z.array(
        z
          .object({
            type: z.string().describe("diet"),
            stime: z.string().describe("饮食记录的开始时间,没指定则为null"),
            etime: z.string().describe("饮食记录的结束时间,没指定则为null"),
            duration: z.string().describe("饮食记录的持续时长,没指定则为null"),

            name: z.string().describe("摄入到体内的东西的名称"),
            value: z
              .number()
              .describe("对应饮食内容的数量,例如:1.5个->value:1.5"),
            unit: z
              .string()
              .describe(
                "根据输入和常识给出对应饮食内容数量的单位,例如3盘->unit:盘"
              ),
            cost: z.number().describe("花费的金额"),
          })
          .required({})
          .describe(
            "如果是一个关于饮食(包括抽烟、喝酒、吃面包、喝水以及吃药等)的记录指令,则用这个模式解析出这个饮食记录的关键内容"
          )
      )
    );
    return dietParser;
  }
  _getSportParser() {
    const sportParser = StructuredOutputParser.fromZodSchema(
      z
        .object({
          type: z.string().describe("sport"),
          stime: z.string().describe("运动的开始时间,没指定则为null"),
          etime: z.string().describe("运动的结束时间,没指定则为null"),
          place: z.string().describe("运动的地点,没有指定则为null"),
          duration: z.string().describe("运动的持续时长,没指定则为null"),
          name: z.array(z.string()).describe("运动的类型"),
          distance: z.number().describe("运动的距离"),
          unit: z.string().describe("运动距离的单位"),
        })
        .required({ name: true })
        .describe(
          "如果是一个有关运动的记录指令(比如跑步、踢球),则用这个模式解析出这个运动记录的关键内容"
        )
    );
    return sportParser;
  }
  _getSignParser() {
    const signParser = StructuredOutputParser.fromZodSchema(
      z
        .object({
          type: z.string().describe("sign"),
          stime: z.string().describe("体征记录的开始时间,没指定则为null"),
          etime: z.string().describe("体征记录的结束时间,没指定则为null"),
          duration: z.string().describe("体征记录的持续时长,没指定则为null"),
          items: z.array(
            z
              .object({
                name: z
                  .string()
                  .describe(
                    "体征的内容,请使用标准术语,如身高、体重、收缩压、舒张压、饭前血糖、左眼视力等。如果没指定血压请根据常识确定舒张压还是收缩压"
                  ),
                value: z.number().describe("对应体征内容的测量数据"),
                unit: z
                  .string()
                  .describe(
                    "对应体征内容测量数据的单位;不能为空,如果没有指定,请根据常识输出单位"
                  ),
                cost: z.number().describe("花费的金额"),
              })
              .describe("体征的内容列表")
          ),
        })
        .required({ items: true })
        .describe(
          "如果是一个关于身体体征情况的记录指令,用这个模式解析出这个体征记录的关键内容"
        )
    );
    return signParser;
  }
  _getFeelParser() {
    return null;
  }
  _getBehaviorParser() {
    const sportParser = StructuredOutputParser.fromZodSchema(
      z.array(
        z
          .object({
            type: z.string().describe("behavior"),
            stime: z.string().describe("开始时间,没指定则为null"),
            etime: z.string().describe("结束时间,没指定则为null"),
            place: z.string().describe("地点,没有指定则为null"),
            duration: z.string().describe("持续时长,没指定则为null"),
            act: z.string().describe("行为的谓语动词"),
            name: z.string().describe("行为的宾语"),
            value: z.number().describe("行为宾语的数量"),
            unit: z.string().describe("行为宾语的单位"),
          })
          .required({ name: true })
          .describe("这个模式解析出这个行为记录的关键内容")
      )
    );
    return sportParser;
  }
  async parseActionUseFewShot(
    action: string,
    input: string,
    account: string,
    conversationId: string
  ) {
    try {
      const chat = this.llmService.getChatOpenAI({});
      console.log("sk:", chat.openAIApiKey);
      console.log(action, input);
      let actionFewShot: FewShotPromptTemplate;
      //let actionSample: FewShotPromptTemplate;
      switch (action) {
        case "diet":
          actionFewShot = await this._getDietFewShot();
          break;
        case "sport":
          actionFewShot = await this._getSportFewShot();
          break;
        case "sign":
          actionFewShot = await this._getSignFewShot();
          break;
        case "feel":
          actionFewShot = await this._getFeelFewShot();
          break;
        case "behavior":
          actionFewShot = await this._getBehaviorFewShot();
          break;
        default:
          null;
      }
      if (actionFewShot == null) {
        return {};
      }
      const chatPrompt = ChatPromptTemplate.fromPromptMessages([
        new HumanMessagePromptTemplate(actionFewShot),
      ]);
      const chain = new LLMChain({
        prompt: chatPrompt,
        llm: chat,
        verbose: false,
      });
      const result = await chain.call({ input });
      console.log("result:", result.text);

      return JSON.parse(result.text);
    } catch (e) {
      this.logger.error(e);
      return {};
    }
  }
  async parseActionUseOutputParser(
    action: string,
    input: string,
    account: string,
    conversationId: string
  ) {
    try {
      const chat = this.llmService.getChatOpenAI({});
      console.log("sk:", chat.openAIApiKey);
      console.log(action, input);
      let actionParser;
      //let actionSample: FewShotPromptTemplate;
      switch (action) {
        case "diet":
          actionParser = this._getDietParser();
          break;
        case "sport":
          actionParser = this._getSportParser();
          break;
        case "sign":
          actionParser = this._getSignParser();
          break;
        case "feel":
          actionParser = this._getFeelParser();
          break;
        case "behavior":
          actionParser = this._getBehaviorParser();
          break;
        default:
          null;
      }
      if (actionParser == null) {
        return {};
      }

      const formatInstructions = actionParser.getFormatInstructions();
      const actionPrompt = new PromptTemplate({
        template: `用以下规则尽可能准确地分析语句,不确定则直接输出null\n{format_instructions}\n{input}`,
        inputVariables: ["input"],
        partialVariables: { format_instructions: formatInstructions },
      });
      const chatPrompt = (
        await actionPrompt.formatPromptValue({ input })
      ).toChatMessages();
      const result = await chat.call(chatPrompt);
      console.log("result:", result);

      return JSON.parse(result.content);
    } catch (e) {
      this.logger.error(e);
      return {};
    }
  }

  async chat({ body, res }) {
    const chat = this.llmService.getChatOpenAI({ response: res });
    //const chat = this.llmService.getChatWenxin({ response: res });
    const messages: Record<string, string>[] = body.messages;
    const account: string = body.account;
    const conversationId: string = body.conversationId;
    try {
      this.logger.log(
        "chat-->",
        "account:",
        account,
        "conversationId:",
        conversationId,
      );
      const lastMessage = messages.pop();
      let systemTemplate =
        "The following is a friendly conversation between a human and an AI. The AI is talkative and provides lots of specific details from its context. If the AI does not know the answer to a question, it truthfully says it does not know.";
      if (messages[0].role == "system") {
        systemTemplate = messages[0].content;
      }
      console.log("system message?", systemTemplate);
      const chatPrompt = ChatPromptTemplate.fromPromptMessages([
        SystemMessagePromptTemplate.fromTemplate(systemTemplate),
        //HumanMessagePromptTemplate.fromTemplate(systemTemplate),
        new MessagesPlaceholder("chat_history"),
        HumanMessagePromptTemplate.fromTemplate("{input}"),
        //new HumanMessagePromptTemplate(prompt)
      ]);
      // Return the current conversation directly as messages and insert them into the MessagesPlaceholder in the above prompt.
      //let memory: BufferMemory = this.memoryMap[account]?.[conversationId];
      let memory = this.memoryMap[account]?.[conversationId];
      if (!memory) {
        memory = new ConversationSummaryMemory({
          memoryKey: "chat_history",
          llm: chat,
          returnMessages: true,

        });
        // memory = new BufferMemory({
        //   returnMessages: true,
        //   memoryKey: "history",
        // });
        this.memoryMap[account] = this.memoryMap[account] || {};
        this.memoryMap[account][conversationId] = memory;
      }
      this.logger.debug(await memory.loadMemoryVariables({}));
      const chain = new ConversationChain({
        memory,
        prompt: chatPrompt,
        llm: chat,
        verbose: false,
      });
      return await chain.call({ input: lastMessage.content });
    } catch (e) {
      this.logger.error("chat error", e);
    }
  }
  async testEs() {
    const chat = this.llmService.getChatOpenAI({});
    const embeddings = this.llmService.getOpenAIEmbeddings();
    const vectorStore = this.vectorStoreService.getElasticSearchVectorStore('test_es', embeddings);
    // const docs = [
    //   new Document({
    //     metadata: { foo: "bar" },
    //     pageContent: "我叫张三",
    //   }),
    //   new Document({
    //     metadata: { foo: "bar" },
    //     pageContent: "java比python更简单",
    //   }),
    //   new Document({
    //     metadata: { baz: "qux" },
    //     pageContent: "中美之间有很多合作领域",
    //   }),
    //   new Document({
    //     metadata: { baz: "qux" },
    //     pageContent:
    //       "我知道很多制作苹果拼盘的技巧，比如切花，摆盘和榨汁",
    //   }),
    // ];

    // const ids = await vectorStore.addDocuments(docs);
    // console.log("ids:", ids);
    const chain = this.chainVectorDBQAService.getVectorDBQAChains(chat, vectorStore, 2);
    //const response = await chain.call({ query: "如何评价中美关系" });
    //const response = await chain.call({ query: "java和python做一下比较" });
    const response = await chain.call({ query: "我叫什么，我知道什么技巧" });
    return response;
  }
}
