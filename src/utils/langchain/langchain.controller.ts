import { Body, Controller, Get, Post, Query, Req, Res } from "@nestjs/common";
import { LangchainService } from "./langchain.service";

@Controller("langchain")
export class LangchainController {
  lastContext = "";
  constructor(private readonly langchainService: LangchainService) { }
  @Get("test_baidu")
  async test_baidu(@Query() query, @Res() res) {
    const isStream = JSON.parse(
      (query.stream == undefined || query.stream == "false") ? "false" : "true"
    );
    if (isStream) {
      res.header("Content-Type", "text/event-stream;charset=utf-8");
    } else {
      res.header("Content-Type", "text/json;charset=utf-8");
    }
    const queryString = query.query ?? "";
    const result = await this.langchainService.test_baidu(queryString, res);
    if (!isStream) {
      res.end(result.content);
    } else {
      res.end();
    }
  }
  @Get("just_test")
  async just_test(@Query() query) {
    const url = query.url ?? "";
    const queryString = query.query ?? "";
    return await this.langchainService.justTest(url, queryString);
  }

  @Get("predict")
  async predict(@Query() query, @Res() res) {
    const isStream = JSON.parse(
      query.stream == undefined || query.stream == "false" ? "false" : "true"
    );
    if (isStream) {
      res.header("Content-Type", "text/event-stream;charset=utf-8");
    } else {
      res.header("Content-Type", "text/json;charset=utf-8");
    }
    const result = await this.langchainService.predict({
      query,
      res,
      isStream,
    });
    if (!isStream) {
      res.end(result);
    } else {
      res.end();
    }
  }
  @Post("chat")
  async chat(@Body() body, @Req() req, @Res() res) {
    const isStream =
      (body.stream == undefined || body.stream == false) ? false : true;

    if (isStream) {
      res.header("Content-Type", "text/event-stream;charset=utf-8");
    } else {
      res.header("Content-Type", "text/json;charset=utf-8");
    }
    //const headers = req.headers;
    const result = await this.langchainService.chat({
      body,
      res
    });
    if (!isStream) {
      res.end(result);
    } else {
      res.end();
    }
  }
  @Post("agent")
  async agent(@Body() body, @Req() req, @Res() res) {
    const isStream =
      body.stream == undefined || body.stream == false ? false : true;
    if (isStream) {
      res.header("Content-Type", "text/event-stream;charset=utf-8");
    } else {
      res.header("Content-Type", "text/json;charset=utf-8");
    }
    //const headers = req.headers;
    const result = await this.langchainService.agent({
      body,
      res,
      isStream,
    });
    if (!isStream) {
      res.end(result);
    } else {
      res.end();
    }
  }
  @Get("fake_stream")
  async fakeStream(@Query() query, @Res() res) {
    res.header("Content-Type", "text/event-stream;charset=utf-8");
    const text = query.text;
    const lowDelay = parseInt(query.lowDelay);
    const highDelay = parseInt(query.highDelay);
    const result = await this.langchainService.fakeStream(text, res,
      lowDelay,
      highDelay,
    );
    res.end();
  }
  @Post("test")
  async test(@Body() body, @Req() req, @Res() res) {
    const isStream =
      body.stream == undefined || body.stream == false ? false : true;
    if (isStream) {
      res.header("Content-Type", "text/event-stream;charset=utf-8");
    } else {
      res.header("Content-Type", "text/json;charset=utf-8");
    }
    //const headers = req.headers;
    const result = await this.langchainService.test({
      body,
      res,
      isStream,
    });
    if (!isStream) {
      res.end(JSON.stringify(result));
    } else {
      res.end();
    }
  }
  @Get("get_action")
  async getAction(@Query() query) {
    const prompt = query.prompt;
    const mode = query.mode || "useOutputParser"; //else 'useFewShot'
    let result;
    if (mode == "useOutputParser") {
      result = await this.langchainService.getActionUseOutputParser(prompt);
    } else {
      result = await this.langchainService.getActionUseFewShot(prompt);
    }
    console.log("get_action:", result);
    return result.properties || result;
  }
  @Get("parse_action")
  async parseAction(@Query() query) {
    const action = query.action;
    const prompt = query.prompt;
    const mode = query.mode || "useOutputParser"; //else 'useFewShot'
    const account = query.account;
    const conversationId = query.conversationId;
    let result;
    if (mode == "useOutputParser") {
      result = await this.langchainService.parseActionUseOutputParser(
        action,
        prompt,
        account,
        conversationId
      );
      console.log("parse_action:", result);
    } else {
      result = await this.langchainService.parseActionUseFewShot(
        action,
        prompt,
        account,
        conversationId
      );
      console.log("parse_action:", result);
    }
    //return result.properties || result;
    return result;
  }
  @Get('es')
  async testEs() {
    return this.langchainService.testEs();
  }
}
