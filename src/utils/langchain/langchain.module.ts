import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EsModule } from 'src/utils/db/es/es.module';
import { DocumentModule } from './document/document.module';
import { LangchainController } from './langchain.controller';
import { LangchainService } from './langchain.service';
import { LLMModule } from './llm/llm.module';
import { VectorStoreModule } from './vector_store/vector_store.module';
import { ChainModule } from './chain/chain.module';

@Module({
  imports: [ConfigModule, EsModule, DocumentModule, VectorStoreModule, LLMModule, ChainModule],
  providers: [LangchainService],
  controllers: [LangchainController],
  exports: [LangchainService]
})
export class LangchainModule { }
