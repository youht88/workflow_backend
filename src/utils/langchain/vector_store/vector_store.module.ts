import { Module } from '@nestjs/common';
import { EsModule } from 'src/utils/db/es/es.module';
import { VectorStoreService } from './vector_store.service';

@Module({
  imports: [EsModule],
  providers: [VectorStoreService],
  exports: [VectorStoreService]
})
export class VectorStoreModule { }
