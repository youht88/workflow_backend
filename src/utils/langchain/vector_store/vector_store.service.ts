import { Injectable } from '@nestjs/common';
import { Document } from 'langchain/document';
import { ElasticClientArgs, ElasticVectorSearch } from 'langchain/vectorstores/elasticsearch';
import { EsService } from 'src/utils/db/es/es.service';
@Injectable()
export class VectorStoreService {
    constructor(private readonly esService: EsService) {
    }
    // to run this first run Elastic's docker-container with `docker-compose up -d --build`
    async getElasticSearchVectorStore(vectorStoreName: string, embeddings) {
        const clientArgs: ElasticClientArgs = {
            client: this.esService.client,
            indexName: vectorStoreName,
        };
        const vectorStore = new ElasticVectorSearch(embeddings, clientArgs);
        const indexExist = await vectorStore.doesIndexExist();
        //console.log("indexExist", indexExist);
        if (!indexExist) {
            await this.esService.create(vectorStoreName, {
                mappings: {
                    embedding: "dense_vector",
                    metadata: "object",
                    text: "text"
                },
                settings: {
                    replicas: 3
                }
            });
        }
        return vectorStore;
    }

    async addDocuments(vectorStore: ElasticVectorSearch, docs: Document<Record<string, any>>[], force?: boolean): Promise<string[]> {
        // Also supports an additional {ids: []} parameter for upsertion
        const hash = docs[0].metadata["sha256"];

        const documentsExist = await this.doseDocumentsExist(vectorStore, hash);

        if (documentsExist) {
            if (force) {
                await this.removeDocumentsWithHash(vectorStore, hash);
            } else {
                return [];
            }
        }

        //const ids = [];
        // for (const [index, doc] of docs.entries()) {
        //     const id = await vectorStore.addDocuments([doc]);
        //     console.log(index, hash, id);
        //     ids.push(id);
        // }
        const ids = await vectorStore.addDocuments(docs);
        return ids;
    }
    async removeDocuments(vectorStore: ElasticVectorSearch, ids: string[]) {
        await vectorStore.delete({ ids });
    }

    async search(vectorStore: ElasticVectorSearch, query: string, n: number): Promise<Document<Record<string, any>>[]> {
        /* Search the vector DB independently with meta filters */
        const results = await vectorStore.similaritySearch(query, n);
        return results;
    }
    async doseDocumentsExist(vectorStore: ElasticVectorSearch, hash: string): Promise<boolean> {
        const vectorStoreName = vectorStore["indexName"];
        const result = await this.esService.query(vectorStoreName, { "term": { "metadata.sha256.keyword": hash } }, { size: 1 });
        return Array.isArray(result) && result.length != 0;
    }
    async removeDocumentsWithHash(vectorStore: ElasticVectorSearch, hash: string) {
        const vectorStoreName = vectorStore["indexName"];
        await this.esService.deleteByQuery(vectorStoreName, { "term": { "metadata.sha256.keyword": hash } });
    }
}
