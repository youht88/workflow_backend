import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { RedisController } from './redis.controller';
import { RedisService } from './redis.service';

@Module({
  imports: [ConfigModule],
  providers: [RedisService],
  controllers: [RedisController],
  exports: [RedisService]
})
export class RedisModule { }
