import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as redis from 'redis';

class A {
    a: string;
    b: string[];
    constructor(a, b) {
        this.a = a;
        this.b = b;
    }
}
@Injectable()
export class RedisService {
    logger: Logger;
    constructor(private readonly config: ConfigService) {
        this.logger = new Logger(RedisService.name);
    }
    async init() {
        const redisURL = `${this.config.get("redis.nodes")[0]}`;
        const client = redis.createClient({ "url": redisURL });
        await client.connect();
        const xxx = new A("abc", ["1", "2"]);
        await client.set("a", JSON.stringify(xxx));
        return client.get("a");
    }
}
