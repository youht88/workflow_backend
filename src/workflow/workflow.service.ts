import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EsService } from 'src/utils/db/es/es.service';

@Injectable()
export class WorkflowService {
    logger: Logger;
    constructor(private readonly config: ConfigService,
        private readonly esService: EsService,
    ) {
        this.logger = new Logger(WorkflowService.name);
        this.init();
    }
    async init() {
        let need_init_workflow: boolean = false;
        need_init_workflow = !(await this.esService.exists("user_profile"));
        this.logger.log(`***** NEED INIT WORKFLOW ${need_init_workflow} ******`);
        if (need_init_workflow) {
            //await this.esService.drop("user_profile");
            await this.esService.create("user_profile", {
                mappings: {
                    account: "keyword",
                    publicKey: "keyword",
                    designMode: "boolean",
                    name: "text",
                    email: "text",
                    avatar: "text",
                    systemMessage: "text",
                    endpoint: "text",
                    useAction: "boolean",
                    conversationTitles: "nested",
                    currentConversationId: "keyword",
                },
                settings: { shards: 1, replicas: 3 },
            });
        }
    }
}