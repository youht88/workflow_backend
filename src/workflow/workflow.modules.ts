import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EsModule } from 'src/utils/db/es/es.module';
import { WorkflowService } from './workflow.service';


@Module({
    imports: [ConfigModule, EsModule],
    providers: [WorkflowService],
})
export class WorkflowModule { }