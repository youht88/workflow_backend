import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EsModule } from 'src/utils/db/es/es.module';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';

@Module({
  imports: [ConfigModule, EsModule],
  controllers: [ProjectController],
  providers: [ProjectService]
})
export class ProjectModule { }
