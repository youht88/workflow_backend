import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import { ProjectService } from "./project.service";

@Controller("v1/project")
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}
  @Post("export")
  async export(@Query() query, @Body() body) {
    const account = query.account;
    const projectId = query.projectId;
    const projectName = body.projectName;
    const content = body.content;
    return await this.projectService.export(
      account,
      projectId,
      projectName,
      content
    );
  }
  @Get("import")
  async import(@Query() query) {
    const account = query.account;
    const projectId = query.projectId;
    return await this.projectService.import(account, projectId);
  }
  @Get("remove")
  async remove(@Query() query) {
    const account = query.account;
    const projectId = query.projectId;
    return await this.projectService.remove(account, projectId);
  }
}
