import { Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { EsService } from "src/utils/db/es/es.service";

@Injectable()
export class ProjectService {
  logger: Logger;
  constructor(
    private readonly config: ConfigService,
    private readonly esService: EsService
  ) {
    this.logger = new Logger(ProjectService.name);
  }
  async create(tableName: string) {
    this.logger.log(`create:${tableName}`);
    await this.esService.create(tableName, {
      mappings: {
        projectId: "keyword",
        projectName: "text",
        createDateTime: "date",
        updateDateTime: "date",
        content: "text",
      },
    });
  }
  async export(
    account: string,
    projectId: string,
    projectName: string,
    content: string
  ) {
    this.logger.log(`export:${account},${projectId}`);
    const projectContent = await this.esService.query(`project_${account}`, {
      term: { projectId },
    });
    if (Array.isArray(projectContent) && projectContent.length != 0) {
      const data = {};
      data["updateDateTime"] = new Date();
      data["projectName"] = projectName;
      data["content"] = content;
      await this.esService.updateByQuery(
        `project_${account}`,
        { term: { projectId } },
        { setting: data }
      );
    } else {
      const data = {};
      data["createDateTime"] = new Date();
      data["updateDateTime"] = new Date();
      data["projectId"] = projectId;
      data["projectName"] = projectName;
      data["content"] = content;
      await this.esService.insert(`project_${account}`, data);
    }
  }
  async import(account: string, projectId: string) {
    this.logger.log(`import:${account},${projectId}`);
    //ensure project_<account> exists
    const tableExists = await this.esService.exists(`project_${account}`);
    if (!tableExists) {
      await this.create(`project_${account}`);
    }
    if (projectId) {
      return await this.esService.query(`project_${account}`, {
        term: { projectId },
      });
    } else {
      return await this.esService.query(`project_${account}`, {});
    }
  }
  async remove(account: string, projectId: string) {
    this.logger.log(`remove:${account},${projectId}`);
    //ensure project_<account> exists
    const tableExists = await this.esService.exists(`project_${account}`);
    if (tableExists) {
      return await this.esService.deleteByQuery(`project_${account}`, {
        term: { projectId },
      });
    }
  }
}
