import { Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { EsService } from "src/utils/db/es/es.service";
import { LangchainService } from "src/utils/langchain/langchain.service";
import { UserService } from "../user/user.service";
import { ConversationTitle } from "./chat.type";

@Injectable()
export class ChatService {
  logger: Logger;
  constructor(
    private readonly config: ConfigService,
    private readonly esService: EsService,
    private readonly langchainService: LangchainService,
    private readonly userService: UserService
  ) {
    this.logger = new Logger(ChatService.name);
  }
  async newConversation(account: string, conversationId: string) {
    this.logger.log(`newConversation:${account},${conversationId}`);
    const data = {};
    data["createDateTime"] = new Date();
    data["updateDateTime"] = new Date();
    data["conversationId"] = conversationId;
    data["chats"] = [];
    console.log("!!!!!!", account, conversationId);
    await this.userService.updateUserProfile(account, {
      currentConversationId: conversationId,
    });
  }
  async addChat(
    account: string,
    conversationId: string,
    data: Record<string, any>
  ) {
    //前端判断如果是conversation的第一条记录，则isNewConversation要设置为true
    this.logger.log(`addChat:${account},${conversationId}`);
    data["conversationId"] = conversationId;
    data["createDateTime"] = data["createDateTime"]
      ? new Date(data["createDateTime"])
      : new Date();
    data["updateDateTime"] = data["updateDateTime"]
      ? new Date(data["updateDateTime"])
      : new Date();
    try {
      await this.esService.insert(`chat_${account}`, data);
    } catch (e) {
      this.logger.error(e);
    }
  }
  async getChats(account: string, conversationId: string) {
    this.logger.log(`getChats:${account},${conversationId}`);
    try {
      const chats = await this.esService.query(
        `chat_${account}`,
        { term: { conversationId: conversationId } },
        { sort: [{ createDateTime: "desc" }] }
      );
      return chats;
    } catch (e) {
      this.logger.error(e);
      return [];
    }
  }
  async getConversationTitles(account: string) {
    this.logger.log(`getConversationTitles:${account}`);
    try {
      const res = await this.esService.query(
        `user_profile`,
        {
          bool: {
            must: [
              { term: { account: account } },
              {
                nested: {
                  path: "conversationTitles",
                  query: { match_all: {} },
                  inner_hits: {
                    size: 100,
                    sort: [{ "conversationTitles.create": { order: "desc" } }],
                  },
                },
              },
            ],
          },
        },
        { nestedPath: "conversationTitles" }
      );
      //const res = await this.esService.query(`user_profile`, { term: { "account": account } }, { fields: ["conversationTitles"] });
      const titles = res?.[0];
      return titles;
    } catch (e) {
      this.logger.error(e);
      return [];
    }
  }
  async insertConversationTitle(account: string, data: ConversationTitle) {
    await this.esService.updateByQuery(
      "user_profile",
      { term: { account: account } },
      {
        script: {
          source: "ctx._source.conversationTitles.add(params.data)",
          lang: "painless",
          params: {
            data: {
              id: data.conversationId,
              title: data.title,
              create: data.create ?? new Date(),
            },
          },
        },
      }
    );
  }
  async newContent(account: string, conversationId: string) {
    this.logger.log(
      `newContent->account:${account},conversationId:${conversationId}`
    );
    if (this.langchainService.memoryMap[account]?.[conversationId]) {
      delete this.langchainService.memoryMap[account][conversationId];
      return null;
    } else {
      return "我已忘记刚才和你说什么了";
    }
  }
  async updateConversationTitle(
    account: string,
    conversationId: string,
    title: string
  ) {
    this.logger.log(
      `updateConversationTitle->account:${account},conversationId:${conversationId},title:${title}`
    );
    // await this.esService.updateByQuery("user_profile",{term:{"account":account}},{})
    await this.esService.updateByQuery(
      `user_profile`,
      { term: { account: account } },
      {
        script: {
          source: `
            if (ctx._source.conversationTitles != null){
              for (int i; i < ctx._source.conversationTitles.length; i++){   
                if (ctx._source.conversationTitles[i].id == params.id){
                  ctx._source.conversationTitles[i].title = params.title;
                  break
                }
              }
            }
            `,
          params: { id: conversationId, title: title },
        },
      }
    );
  }
  async removeConversation(account: string, conversationId: string) {
    this.logger.log(
      `removeConversation->account:${account},conversationId:${conversationId}`
    );
    await this.esService.deleteByQuery(`chat_${account}`, {
      term: { conversationId: conversationId },
    });
    try {
      delete this.langchainService.memoryMap[account][conversationId];
    } catch (e) {
      this.logger.log(`没有${account}的memory`);
    }
    await this.esService.updateByQuery(
      `user_profile`,
      { term: { account: account } },
      {
        script: {
          source:
            "ctx._source.conversationTitles.removeIf(item -> item.id == params.id)",
          params: { id: conversationId },
        },
      }
    );
    return "ok";
  }
}
