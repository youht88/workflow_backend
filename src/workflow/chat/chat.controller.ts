import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ChatService } from './chat.service';
import { ConversationTitle } from './chat.type';

@Controller('v1/chat')
export class ChatController {
    constructor(private readonly chatService: ChatService) { };
    @Get('new_conversation')
    async newConversation(@Query() query) {
        const account = query.account;
        const conversationId = query.conversationId;
        return await this.chatService.newConversation(account, conversationId);
    }
    @Post('add_chat')
    async addChat(@Query() query, @Body() body) {
        const account = query.account;
        const conversationId = query.conversationId;
        const isNewConversation = query.isNewConversation ?? false;
        return await this.chatService.addChat(account, conversationId, body);
    }
    @Get('get_chats')
    async getChats(@Query() query) {
        const account = query.account;
        const conversationId = query.conversationId;
        return await this.chatService.getChats(account, conversationId);
    }
    @Post('insert_conversation_title')
    async insertConversationTitle(@Query() query, @Body() body: ConversationTitle) {
        const account = query.account;
        return await this.chatService.insertConversationTitle(account, body);
    }
    @Get('get_conversation_titles')
    async getConversationTitles(@Query() query) {
        const account = query.account;
        return await this.chatService.getConversationTitles(account);
    }
    @Get('update_conversation_title')
    async updateConversationTitle(@Query() query) {
        const account = query.account;
        const conversationId = query.conversationId;
        const title = query.title;
        return await this.chatService.updateConversationTitle(account, conversationId, title);
    }
    @Get('new_content')
    async newContent(@Query() query) {
        const account = query.account;
        const conversationId = query.conversationId;
        return await this.chatService.newContent(account, conversationId);
    }
    @Get('remove_conversation')
    async removeConversation(@Query() query) {
        const account = query.account;
        const conversationId = query.conversationId;
        return await this.chatService.removeConversation(account, conversationId);
    }
}
