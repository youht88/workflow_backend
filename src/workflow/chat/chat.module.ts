import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EsModule } from 'src/utils/db/es/es.module';
import { LangchainModule } from 'src/utils/langchain/langchain.module';
import { UserModule } from '../user/user.module';
import { ChatController } from './chat.controller';
import { ChatService } from './chat.service';

@Module({
    imports: [ConfigModule, EsModule, LangchainModule, UserModule],
    controllers: [ChatController],
    providers: [ChatService],
    exports: [ChatService]
})
export class ChatModule { }
