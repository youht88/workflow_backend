import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EsService } from 'src/utils/db/es/es.service';
import { LangchainService } from 'src/utils/langchain/langchain.service';

@Injectable()
export class UserService {
    logger: Logger;
    constructor(private readonly config: ConfigService,
        private readonly esService: EsService,
        private readonly langchainService: LangchainService,
    ) {
        this.logger = new Logger(UserService.name);
    }
    async getUserProfile(account: string) {
        this.logger.log(`getUserProfile:${account}`);
        const res = await this.esService.query("user_profile", { term: { "account": account } });
        console.log(res)
        return res
    }
    async setUserProfile(data: Record<string, any>) {
        const account: string = data.account;
        this.logger.log(`setUserProfile:${account}`);
        data['createDateTime'] = new Date();
        data['updateDateTime'] = new Date();
        await this.createAccountChat(account)
        return this.esService.insert("user_profile", data);
    }
    async updateUserProfile(account: string, data: Record<string, any>) {
        this.logger.log(`updateUserProfile:${account},data->${JSON.stringify(data)}`);
        data['updateDateTime'] = new Date();
        return this.esService.updateByQuery("user_profile", { term: { "account": account } }, { setting: data });
    }
    async createAccountChat(account: string) {
        await this.esService.create(`chat_${account}`,
            {
                mappings: {
                    "conversationId": 'keyword',
                    "createDateTime": "date",
                    "updateDateTime": "date",
                    "role": "keyword",
                    "type": "keyword",
                    "content": "text"
                }, settings: { replicas: 3 }
            });
        await this.esService.create(`project_${account}`,
            {
                mappings: {
                    'projectId': 'keyword',
                    'projectName': 'text',
                    'createDateTime': 'date',
                    'updateDateTime': 'date',
                    'content': 'text'
                }
            });
    }
    async resetUser(account: string) {
        await this.esService.deleteByQuery('user_profile', { "term": { "account": account } });
        await this.esService.drop(`chat_${account}`);
        await this.esService.drop(`project_${account}`);
        return `RESET user account:${account}`;
    }
}
