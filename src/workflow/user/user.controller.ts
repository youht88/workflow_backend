import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('v1/user')
export class UserController {
    constructor(private readonly userService: UserService) { };
    @Get('get_user_profile')
    async getUesrProfile(@Query() query) {
        const account = query.account;
        return await this.userService.getUserProfile(account);
    }
    @Post('set_user_profile')
    async setUserProfile(@Body() body) {
        return await this.userService.setUserProfile(body);
    }
    @Post('update_user_profile')
    async updateUserProfile(@Query() query, @Body() body) {
        const account = query.account;
        return await this.userService.updateUserProfile(account, body);
    }
    @Get('reset_user')
    async resetUser(@Query('account') account) {
        return await this.userService.resetUser(account);
    }
}
