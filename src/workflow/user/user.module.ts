import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EsModule } from 'src/utils/db/es/es.module';
import { LangchainModule } from 'src/utils/langchain/langchain.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [ConfigModule, EsModule, LangchainModule],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule { }