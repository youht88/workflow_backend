FROM ubuntu

ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Create app directory
WORKDIR /home

#RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apt update
#RUN apk add chromium
RUN apt install nodejs npm -y
RUN apt install xvfb -y
RUN apt install g++ make -y
RUN apt install python3 -y
RUN apt install cmake -y
#RUN npm install -g cnpm --registry=https://registry.npm.taobao.org
# 升级到最新的nodejs
RUN apt install curl wget -y
RUN npm install n -g
RUN n stable
RUN hash -r

RUN apt install sqlite3 -y
RUN npm install puppeteer-core

COPY package.json ./
RUN npm install

#puppeteer
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt-get -y install xvfb gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 \
      libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 \
      libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 \
      libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 \
      libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget \
      fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf 
RUN apt update
RUN apt install -y -f libgbm1  libu2f-udev libvulkan1 libindicator7
RUN dpkg -i google-chrome-stable_current_amd64.deb
#
COPY . /home

ENV DISPLAY=:99 
EXPOSE 3000
#CMD sh -c "top"
CMD sh -c "Xvfb :99 -screen 0 800x600x24 -ac & npm start"
